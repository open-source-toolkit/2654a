# Python音乐推荐系统（协同过滤推荐算法）

## 项目简介

本项目是一个基于Django框架和MySQL/SQLite3数据库的Python音乐推荐系统。该系统采用了协同过滤推荐算法，结合用户画像进行个性化音乐推荐。通过分析用户的历史行为和偏好，系统能够为用户提供更加精准的音乐推荐列表。

## 开发环境

- **开发工具**: PyCharm
- **数据库**: MySQL/SQLite3
- **编程语言**: Python 3.x
- **Web框架**: Django

## 功能特点

1. **协同过滤推荐**: 系统采用基于用户的协同过滤算法，结合用户画像进行推荐，提高推荐列表的准确性和成熟度。
2. **跨平台支持**: 系统在Windows平台上搭建，但同样适用于其他操作系统。
3. **数据存储**: 使用MySQL或SQLite3数据库进行数据的存储和管理。
4. **数据集**: 使用Kaggle平台上的KKBoxs Music Recommendation Challenge比赛的公开数据集，该数据集包含了Last.fm Dataset-360K Users的详细信息。
5. **SVD矩阵分解**: 针对数据集使用SVD矩阵分解进行相似相关度的计算分析，预测用户对音乐的评分，从而进行个性化推荐。

## 数据集

- **数据来源**: Kaggle平台上的KKBoxs Music Recommendation Challenge比赛。
- **数据内容**: 包含超过3000万首音乐曲目，涵盖了亚洲领先的音乐流媒体服务提供商KKBox的全面音乐库。

## 安装与运行

1. **克隆仓库**:
   ```bash
   git clone https://github.com/yourusername/music-recommendation-system.git
   cd music-recommendation-system
   ```

2. **安装依赖**:
   ```bash
   pip install -r requirements.txt
   ```

3. **配置数据库**:
   - 修改`settings.py`中的数据库配置，选择使用MySQL或SQLite3。
   - 运行数据库迁移命令：
     ```bash
     python manage.py migrate
     ```

4. **导入数据集**:
   - 将数据集文件放置在项目目录下，并运行数据导入脚本。

5. **启动服务器**:
   ```bash
   python manage.py runserver
   ```

6. **访问系统**:
   - 打开浏览器，访问`http://127.0.0.1:8000/`即可进入音乐推荐系统。

## 贡献

欢迎大家贡献代码、提出问题或建议。请通过GitHub的Issue和Pull Request功能进行交流。

## 许可证

本项目采用MIT许可证，详情请参阅`LICENSE`文件。

---

希望这个音乐推荐系统能够帮助你更好地理解和应用协同过滤推荐算法，同时也为音乐爱好者提供一个个性化的音乐推荐平台。